module Analyzer where 
import System.IO
import System.Environment
import qualified Data.Map as M
import Control.Monad.Error
import Control.Monad.Reader
import Control.Monad.State
import Control.Monad.Writer
import Data.Maybe
import LexSimpleC
import ParSimpleC
import AbsSimpleC
import ErrM

-- main monad for Analyzer
type CheckM a = ErrorT String (StateT CheckEnv (WriterT CompiledCode IO)) a

--it is not type but data because of better Show behavior 
--for debugging e.g. '(Pointer 2)' tells more than just '2'
data Pointer = Pointer Int deriving (Eq,Ord,Show)
data Label = Label Int deriving (Eq,Ord,Show)
type Variable = (Type, Pointer)

data CheckEnv = CheckEnv {
  variablesMap :: M.Map Ident Variable,
  pointersCounter :: Pointer, 
  labelsCounter :: Label, 
  functionsMap :: M.Map Ident FuncInfo,
  curFunc :: Ident
  } deriving (Show)

data FuncInfo = FuncInfo {  
  funcStart :: Label, 
  funcParamTypes :: [Type],
  funcResType :: Type
  } deriving (Show)

emptyCheckEnv :: CheckEnv
emptyCheckEnv = CheckEnv {
  variablesMap = M.empty, 
  pointersCounter = Pointer 0, 
  labelsCounter = Label 0, 
  functionsMap = M.empty,
  curFunc = Ident ""
}

-- result of analyzer - code compiled to Intermediate Language
-- Intermediate Language - list of Instuctions grouped into sub-lists by Labels
type CompiledCode = [(Label, Instruction)]
-- instructons of Intermediate Language
data Instruction = IPrint 
    | IAssign Pointer |  IDeclVar Type Pointer | IConst Constant | IGetVar Pointer 
    | IJump Label | IIfThen Label Label | IIfThenElse Label Label Label 
    | IOr | IAnd | INot 
    | ILess | IGreater | IEqual | INotEqual | ILessEqual | IGreaterEqual 
    | IPlus | IMinus | IMult | IDiv | IMod 
    | ICallFunc Label Label | IFinish
    deriving (Eq,Ord,Show)

runAnalyzer::String->IO (Either String CompiledCode)
runAnalyzer file = do 
  case pProgram (myLexer file) of 
    Bad a -> return$ Left ("Lexical error: " ++ a)
    Ok tree -> do        
      ((err, env), flow) <- (runChecker emptyCheckEnv tree)
      case (err) of
        (Left e) -> return$ Left ("Type mismatch error: " ++ e)
        (Right _) -> return $ (Right flow)

runChecker :: CheckEnv -> Program -> IO ((Either String (), CheckEnv), CompiledCode)
runChecker env tree = runWriterT (runStateT (runErrorT (checkProgram tree)) env)

failure :: Show a => a -> CheckM()
failure x = throwError ("Not implemented functionality: " ++ show x)

failType :: Show a => a -> CheckM Type
failType x = throwError ("Not implemented functionality: " ++ show x)

getVariables :: CheckM (M.Map Ident Variable)
getVariables = do
  env <- get 
  return$ variablesMap env

putVariables :: (M.Map Ident Variable) -> CheckM ()
putVariables mem = do
  env <- get  
  put env { variablesMap = mem }

getFuncInfo:: Ident -> CheckM (FuncInfo)
getFuncInfo id = do
  env <- get  
  case (M.lookup id (functionsMap env)) of
    Nothing -> let (Ident n) = id in throwError ("undefined function " ++ show n)
    Just res -> return res

putFuncInfo:: Ident -> FuncInfo -> CheckM ()
putFuncInfo id info = do
  env <- get
  put env { functionsMap = (M.insert id info (functionsMap env)) }

addVar:: Ident -> Type -> CheckM (Pointer)
addVar name type' = do
  env <- get
  let Pointer ptr = pointersCounter env
  let vars = variablesMap env
  put env { variablesMap = M.insert name (type', Pointer (ptr + 1)) vars, 
            pointersCounter = Pointer (ptr + 1) }  
  return $ Pointer (ptr + 1)

getVar :: Ident -> CheckM Variable
getVar name = do  
  env <- get
  case (M.lookup name (variablesMap env)) of
    Nothing -> let (Ident n) = name in throwError ("unbound variable " ++ show n)
    Just res -> return res


getLabel:: CheckM Label
getLabel = do
  env <- get
  return$ labelsCounter env

addLabel:: CheckM Label
addLabel = do
  env <- get  
  let Label lCounter = (labelsCounter env)
  put env { labelsCounter = Label (lCounter + 1) }
  return$ Label (lCounter + 1)

addInstructionLab::Label -> Instruction -> CheckM()
addInstructionLab label inst = tell [(label, inst)]

addInstruction::Instruction -> CheckM()
addInstruction inst = do
  label <- getLabel
  tell [(label, inst)]

checkProgram :: Program -> CheckM()
checkProgram x = case x of
  Progr ex  -> do
    addInstruction IFinish
    addLabel
    forM_ ex checkGlobalVars
    forM_ ex checkFunc_declaration
    func <- getFuncInfo (Ident "main")
    -- run main
    addInstructionLab (Label 1) (ICallFunc (funcStart func) (Label 0))

checkGlobalVars :: External_declaration -> CheckM()
checkGlobalVars x =  case x of
  Global dec -> checkDec dec
  Afunc type' id params block -> return() --will be checked later

checkFunc_declaration :: External_declaration -> CheckM()
checkFunc_declaration x = case x of
  Global dec  -> return ()        --was already checked
  Afunc type' id params block -> do
    lab <- addLabel
    paramTypes <- forM params (checkParameter_declaration id)    
    putFuncInfo id FuncInfo {
      funcStart = lab,
      funcParamTypes = paramTypes, 
      funcResType = type'
    }
    env <- get 
    put env { curFunc = id }
    
    --do something with params
    checkBlock block    

checkParameter_declaration :: Ident -> Parameter_declaration -> CheckM(Type)
checkParameter_declaration id (TypeAndParam type' declarator) = do
  checkDeclarator type' declarator
  env <- get
  let ptr = pointersCounter env  
  addInstruction (IAssign ptr)
  return(type')

checkDec :: Dec -> CheckM()
checkDec x = case x of
  Declarators type' decls -> do
    forM_ decls (checkDeclarator type')
    return ()

checkDeclarator :: Type -> Declarator -> CheckM()
checkDeclarator type' x = case x of
  Name id -> do        
    p <- addVar id type'  
    addInstruction (IDeclVar type' p)
  Array declarator constant_expression  -> failure x    

checkStm :: Stm -> CheckM()
checkStm x = case x of
  SelS selection_stm -> checkSelection_stm selection_stm
  IterS iter_stm  -> checkIter_stm iter_stm
  NormalS normal_stm  -> checkNormal_stm normal_stm

checkThen_Stm :: Then_Stm -> CheckM()
checkThen_Stm stm = checkStm $ convert_then_stm stm  

convert_then_stm:: Then_Stm -> Stm
convert_then_stm s = case s of
  (SSelThen exp then_s stm) -> SelS $ SselTwo exp then_s (convert_then_stm stm)
  (SwhileThen exp then_s) -> IterS $ SiterWhile exp (convert_then_stm then_s)
  (SForThem expr1 expr2 exp3 stm4) -> IterS $ SiterFor expr1 expr2 exp3 (convert_then_stm stm4)
  (NormalSThen stm) -> NormalS stm

checkNormal_stm :: Normal_stm -> CheckM()
checkNormal_stm x = case x of
  CompS block  -> checkBlock block
  ExprS expr  -> checkExpr expr
  JumpS return_stm  -> checkReturn_stm return_stm
  PrintS ex  -> do
    t <- checkExp ex 
    case t of
      Tvoid -> throwError "function 'print' have wrong type of argument"
      _ -> addInstruction (IPrint)

checkBlock :: Block -> CheckM()
checkBlock x = case x of
  BlockOne -> return ()
  BlockTwo stms -> checkStatements stms
  BlockThree decs -> do
    oldMem <- getVariables
    forM_ decs checkDec
    putVariables oldMem
  BlockFour decs stms -> do
    oldMem <- getVariables
    forM_ decs checkDec
    checkStatements stms
    putVariables oldMem

checkStatements :: [Stm] -> CheckM()
checkStatements [] = return()
checkStatements ((NormalS (JumpS s)):ls) = --if statement is return
  checkReturn_stm s
checkStatements (s:ls) = do
  checkStm s
  checkStatements ls

checkExpr :: Expr -> CheckM()
checkExpr x = case x of
  SexprOne  -> return ()
  SexprTwo exp  -> do 
    checkExp exp 
    return ()

checkSelection_stm :: Selection_stm -> CheckM()
checkSelection_stm x = case x of
  SselOne ex stm  -> do
    t <- checkExp ex
    case t of
      Tbool -> do  
        cur_l <- getLabel
        then_l <- addLabel
        checkStm stm
        endif_l <- addLabel
        addInstructionLab cur_l (IIfThen then_l endif_l)
      _ -> throwError "Not a boolean value is passed to if statement"
  SselTwo ex then_stm stm  -> do
    t <- checkExp ex
    case t of
      Tbool -> do
        cur_l <- getLabel
        then_l <- addLabel
        checkThen_Stm then_stm
        else_l <- addLabel
        checkStm stm
        endif_l <- addLabel
        addInstructionLab cur_l (IIfThenElse then_l else_l endif_l)
      _ -> throwError "Not a boolean value is passed to if statement"

checkIter_stm :: Iter_stm -> CheckM()
checkIter_stm x = case x of
  SiterWhile ex stm  -> do
    cur_l <- getLabel
    condition_l <- addLabel
    t <- checkExp ex
    case t of
      Tbool -> do
        inner_l <- addLabel
        checkStm stm  
        addInstruction (IJump condition_l)              
        end_l <- addLabel
        addInstructionLab cur_l (IJump condition_l)
        addInstructionLab condition_l (IIfThen inner_l end_l)
      _ -> throwError "Not a boolean value is passed to while condition"
  SiterFor expr1 expr2 exp3 stm4  -> do
    oldMem <- getVariables
    checkExpr expr1
    let whileExpr = NormalS $ CompS $ BlockTwo (stm4:[NormalS $ ExprS $ SexprTwo exp3])
    case expr2 of
      (SexprTwo innerExp) -> checkIter_stm (SiterWhile innerExp whileExpr)
      SexprOne -> checkIter_stm (SiterWhile (Econst (Ebool BoolTrue)) whileExpr)
    putVariables oldMem

checkReturn_stm :: Return_stm -> CheckM()
checkReturn_stm x = case x of
  Return exp -> do
    t1 <- checkExp exp
    env <- get
    funcInfo <- getFuncInfo (curFunc env)
    let t2 = (funcResType funcInfo)    
    case (t1 == t2) of
      True -> return() -- correct type
      False -> do
        env <- get
        let Ident funId = curFunc env
        throwError$ "function " ++ (show funId) ++ " returns value of wrong type"      

checkExp :: Exp -> CheckM Type
checkExp x = case x of
  Ecomma exp1 exp2  -> do
    checkExp exp1
    checkExp exp2
    return Tvoid
  Eassign exp1 op exp3  -> do
    (t1, p) <- checkLvalueExp exp1
    t2 <- checkExp exp3
    checkAssignment_op op p    
    case (t1 == t2) of
      True -> addInstruction (IAssign p) >> return Tvoid
      False -> throwError "type error in assignment"
  Elor exp1 exp2  -> checkExp2B exp1 exp2 IOr
  Eland exp1 exp2  -> checkExp2B exp1 exp2 IAnd  
  Eeq exp1 exp2  -> checkBoolExp2 exp1 exp2 IEqual
  Eneq exp1 exp2  -> checkBoolExp2 exp1 exp2 INotEqual
  Elthen exp1 exp2 -> checkBoolExp2 exp1 exp2 ILess
  Egrthen exp1 exp2 -> checkBoolExp2 exp1 exp2 IGreater  
  Ele exp1 exp2  -> checkBoolExp2 exp1 exp2 ILessEqual
  Ege exp1 exp2  -> checkBoolExp2 exp1 exp2 IGreaterEqual
  Eplus exp1 exp2  -> checkIntExp2 exp1 exp2 IPlus  
  Eminus exp1 exp2   -> checkIntExp2 exp1 exp2 IMinus
  Etimes exp1 exp2  -> checkIntExp2 exp1 exp2 IMult
  Ediv exp1 exp2  -> checkIntExp2 exp1 exp2 IDiv
  Emod exp1 exp2  -> checkIntExp2 exp1 exp2 IMod
  Econst c  -> do
    addInstruction (IConst c) 
    checkConstant c
  Epreop unary_operator exp -> checkUnary_operator unary_operator exp
  Efunk id -> checkExp (Efunkpar id [])
  Efunkpar id exps -> do    
    func <- getFuncInfo id
    let startLabel = funcStart func
    let fType = funcResType func        
    let paramTypes = funcParamTypes func
    
    checkParams id (reverse exps) (reverse paramTypes)

    cur_l <- getLabel
    next_l <- addLabel    
    addInstructionLab cur_l (ICallFunc startLabel next_l)
    return fType
  _ -> do
    (t, p) <- checkLvalueExp x
    return t 

checkParams:: Ident -> [Exp] -> [Type] -> CheckM()
checkParams _ [] [] = return()
checkParams (Ident id) [] (t:ts) = throwError$ "call of function " ++ (show id) 
  ++ " with wrong number of arguments"
checkParams (Ident id) (e:es) [] = throwError$ "call of function " ++ (show id) 
  ++ " with wrong number of arguments"
checkParams id (exp:ls) (t:ts) = do  
  t2 <- checkExp exp  
  case (t == t2) of
      True -> checkParams id ls ts
      False -> do
        let Ident id_num = id       
        throwError$ "arguments of function " ++ (show id_num) ++ " have wrong types"

checkExp2B::Exp -> Exp -> Instruction -> CheckM Type
checkExp2B exp1 exp2 ins = do  
    t1 <- checkExp exp1
    t2 <- checkExp exp2
    case (t1, t2) of
      (Tbool, Tbool) -> addInstruction (ins) >> return Tbool
      _ -> throwError$ "expression '"++ (getIntrName ins) ++ "' requires two bool argumets"

checkIntExp2:: Exp -> Exp -> Instruction -> CheckM Type
checkIntExp2 exp1 exp2 ins = do  
    t1 <- checkExp exp1
    t2 <- checkExp exp2
    case (t1, t2) of
      (Tint, Tint) -> addInstruction (ins) >> return Tint
      _ -> throwError$ "expression '"++ (getIntrName ins) ++ "' requires two int argumets"

checkBoolExp2:: Exp -> Exp -> Instruction -> CheckM Type
checkBoolExp2 exp1 exp2 ins = do  
    t1 <- checkExp exp1
    t2 <- checkExp exp2
    case (t1, t2) of
      (Tint, Tint) -> addInstruction (ins) >> return Tbool
      (Tbool, Tbool) -> addInstruction (ins) >> return Tbool
      _ -> throwError$ "wrong arguments of expression '" ++ (getIntrName ins) ++ "'"

getIntrName:: Instruction -> String
getIntrName instr = let (n:name) = (show instr) in name


checkLvalueExp :: Exp -> CheckM Variable
checkLvalueExp x = case x of
  --Epreinc exp  -> failType x
  --Epredec exp  -> failType x  
  --Earray exp1 exp2  -> failType x 
  --Epostinc exp  -> failType x
  --Epostdec exp  -> failType x
  Evar id -> do
    (t, p) <- getVar id
    addInstruction (IGetVar p) 
    return (t, p)
  _ -> throwError ("Not implemented case: " ++ show x)  

checkConstant :: Constant -> CheckM Type
checkConstant x = case x of
  Eint n  -> return Tint
  Ebool bool_const  -> return Tbool

checkUnary_operator :: Unary_operator -> Exp -> CheckM Type
checkUnary_operator op exp = case op of
  Plus -> do 
    type' <- checkExp exp
    check_type type' Tint "wrong argument of unary plus operator"   
  Negative -> do
    addInstruction (IConst $ Eint 0)
    type' <- checkExp exp  
    check_type type' Tint "wrong argument of unary minus operator" 
    addInstruction IMinus 
    return type' 
  Logicalneg -> do    
    type' <- checkExp exp  
    check_type type' Tbool "wrong argument of logical negation operator"  
    addInstruction INot
    return type'
check_type:: Type -> Type -> String -> CheckM Type
check_type t t2 errorStr | t == t2 = return t
                         | otherwise = throwError errorStr

checkAssignment_op :: Assignment_op -> Pointer -> CheckM() 
checkAssignment_op op p = case op of
  Assign  -> return()
  AssignMul  -> addInstruction (IMult)
  AssignDiv  -> addInstruction (IDiv)
  AssignMod  -> addInstruction (IMod)
  AssignAdd  -> addInstruction (IPlus)
  AssignSub  -> addInstruction (IMinus)
  AssignAnd  -> addInstruction (IAnd)  
  AssignOr  -> addInstruction (IOr)