import Analyzer
import System.IO
import System.Environment
import qualified Data.Map as M
import Control.Monad.Error
import Control.Monad.Reader
import Control.Monad.State
import Data.List
import Data.Maybe
import AbsSimpleC

{- imported from Compiler module Instructions: 
data Instruction = IPrint 
    | IAssign Pointer |  IDeclVar Type Pointer | IConst Constant | IGetVar Pointer 
    | IJump Label | IIfThen Label Label | IIfThenElse Label Label Label 
    | IOr | IAnd | INot 
    | ILess | IGreater | IEqual | INotEqual | ILessEqual | IGreaterEqual 
    | IPlus | IMinus | IMult | IDiv | IMod 
    | ICallFunc Label Label | IFinish
    deriving (Eq,Ord,Show)
-}

type EvalM = ErrorT String (ReaderT Labels (StateT Memory IO))
data Memory = Memory { 
  callStack :: CallStack, 
  stack :: Stack Constant 
} deriving (Show)

type CallStack = Stack (M.Map Pointer Constant)

type Stack a = [a]
type Labels = M.Map Label [Instruction]

main::IO()
main = do  
  name <- getProgName
  arguments <- getArgs
  case arguments of    
    [n] -> readFile n >>= run
    _ -> hPutStrLn stderr $ "Error: " ++ unwords(name:arguments)

run::String->IO()
run file =  do
  flow <- runAnalyzer file
  case flow of
    (Left e) -> hPutStrLn stderr e
    (Right flow) -> do
      res <- runEval emptyMemory (flowToMap flow M.empty)
      case res of
        (Left e) -> hPutStrLn stderr ("Runtime error: " ++ e)
        (Right ()) -> return()

runEval :: Memory -> Labels -> IO (Either String ())
runEval mem labels = do
  putStrLn ("\n\n" ++ (showInstr (M.toList labels)))  
  evalStateT (runReaderT (runErrorT $ eval $ fromJust 
    $ M.lookup (Label 1) labels) labels) mem

flowToMap :: CompiledCode -> Labels -> Labels
flowToMap [] l = (M.map reverse l)
flowToMap ((index, ins):fx) l = case (M.lookup index l) of
  Just list -> flowToMap fx (M.insert index (ins:list) l)
  Nothing -> flowToMap fx (M.insert index [ins] l)

emptyMemory :: Memory
emptyMemory = Memory { callStack = [M.empty], stack = emptyStack }

emptyStack :: Stack a
emptyStack = []

push' :: a -> Stack a -> Stack a
push' x xs = (x:xs)

pop' :: Stack a -> (a, Stack a)
pop' (x:xs) = (x, xs)

pop :: EvalM Constant
pop = do
  mem <- get 
  let (val, stack') = pop' (stack mem)
  put mem { stack = stack' }
  return val

pop_two :: EvalM (Constant, Constant)
pop_two = do
  i1 <- pop
  i2 <- pop
  return (i2, i1)

push :: Constant -> EvalM ()
push val = do
  mem <- get 
  let stack' = push' val (stack mem)
  put mem { stack = stack' }

value :: Pointer -> EvalM Constant
value pointer = do
  env <- get
  let (glob:mem) = callStack env
  case mem of
    [] -> return $ fromJust (M.lookup pointer glob) 
    (cur:ls) -> case M.lookup pointer cur of
      Just v -> return v    
      Nothing -> return $ fromJust (M.lookup pointer glob) 

callStackPush:: EvalM()
callStackPush = do
  env <- get
  let (glob:ls) = callStack env  
  put env { callStack = (glob:(M.empty):ls) }

callStackPop:: EvalM()
callStackPop = do
  env <- get
  let (glob:cur:ls) = callStack env    
  put env { callStack = (glob:ls) }

update :: Pointer -> Constant -> EvalM ()
update p c = do  
  env <- get
  let (glob:mem) = callStack env
  case mem of
    [] -> put env { callStack = ((M.insert p c glob):mem) }
    (cur:ls) -> case M.lookup p cur of
      Just v -> put env { callStack = (glob:(M.insert p c cur):ls) }
      Nothing -> put env { callStack = ((M.insert p c glob):mem) }

insertVar :: Pointer -> Constant -> EvalM ()
insertVar p c = do  
  env <- get
  let (glob:mem) = callStack env
  case mem of
    [] -> put env { callStack = ((M.insert p c glob):mem) }
    (cur:ls) -> put env { callStack = (glob:(M.insert p c cur):ls) }

evalBool :: Bool_const -> Bool
evalBool b = case b of
  BoolTrue -> True
  BoolFalse -> False

hBoolToEbool :: Bool -> Bool_const
hBoolToEbool b = case b of
  True -> BoolTrue
  False -> BoolFalse

getBlockByLabel :: Label -> EvalM [Instruction]
getBlockByLabel l = do
  labels <- ask
  case M.lookup l labels of
    Just inst -> return inst
    Nothing -> return []

eval :: [Instruction] -> EvalM()
eval [] = return ()
eval (inst:xs) = do
  case inst of  
    (IDeclVar t p) -> case t of
      Tbool -> insertVar p (Ebool BoolFalse)
      Tint -> insertVar p (Eint 0)
    (IGetVar p) -> do
      v <- value p 
      push v
    (IConst c) -> push c
    IPrint -> do
      v <- pop
      liftIO $ print v      
    (IAssign pointer) -> do
      v <- pop
      update pointer v
    (IIfThen then_l endif_l) -> do
      Ebool v <- pop
      case v of
        BoolTrue -> do 
          then_block <- getBlockByLabel then_l
          eval then_block
          return ()
        BoolFalse -> do
          end_block <- getBlockByLabel endif_l
          eval end_block
          return ()
    (IIfThenElse then_l else_l endif_l) -> do
      Ebool v <- pop
      end_block <- getBlockByLabel endif_l
      case v of
        BoolTrue -> do 
          then_block <- getBlockByLabel then_l
          eval then_block
          eval end_block
          return ()
        BoolFalse -> do
          else_block <- getBlockByLabel else_l
          eval else_block
          eval end_block
          return ()
    (IJump label)-> do
      block <- getBlockByLabel label
      eval block
      return ()
    INot -> do
      Ebool v <- pop
      push $ Ebool $ hBoolToEbool $ not $ evalBool v
    IOr -> evalBoolTwoArgs (\(Ebool a, Ebool b) -> ((evalBool a) || (evalBool b)))
    IAnd -> evalBoolTwoArgs (\(Ebool a, Ebool b) -> ((evalBool a) && (evalBool b)))
    ILess -> evalBoolTwoArgs (\(Eint a, Eint b) -> (a < b))
    IGreater -> evalBoolTwoArgs (\(Eint a, Eint b) -> (a > b))
    ILessEqual -> evalBoolTwoArgs (\(Eint a, Eint b) -> (a <= b))
    IGreaterEqual -> evalBoolTwoArgs (\(Eint a, Eint b) -> (a >= b))
    IEqual -> evalBoolTwoArgs (\(Eint a, Eint b) -> (a == b))
    INotEqual -> evalBoolTwoArgs (\(Eint a, Eint b) -> (a /= b))
    IPlus -> evalTwoArgs (\(Eint a, Eint b) -> Eint (a + b))      
    IMinus -> evalTwoArgs (\(Eint a, Eint b) -> Eint (a - b))
    IMult -> evalTwoArgs (\(Eint a, Eint b) -> Eint (a * b))
    IDiv -> do
      (Eint a, Eint b) <- pop_two
      case (b == 0) of
        False -> push (Eint (a `div` b))
        True -> throwError "Division by zero"
    IMod -> do
      (Eint a, Eint b) <- pop_two
      case (b == 0) of
        False -> push (Eint (a `mod` b))
        True -> throwError "Division by zero"
    (ICallFunc funcStartLabel next_l) -> do          
      callStackPush      
      block <- getBlockByLabel funcStartLabel
      eval block      
      callStackPop      
      n_block <- getBlockByLabel next_l
      eval n_block
    IFinish -> return()
  eval xs

evalTwoArgs :: ((Constant, Constant) -> Constant) -> EvalM()
evalTwoArgs func = do
  pair <- pop_two
  push (func pair)

evalBoolTwoArgs :: ((Constant, Constant) -> Bool) -> EvalM()
evalBoolTwoArgs func = do
  pair <- pop_two
  push (Ebool (hBoolToEbool (func pair)))

showInstr :: [(Label, [Instruction])] -> String
showInstr = intercalate "\n " . map showI
  where showI (i, l) = (show i) ++ "\n    " ++ 
          ((intercalate "\n    " . map show) l)