all:
	ghc --make interpreter
clean:
	-rm -f *.log *.aux *.hi *.o *.dvi *.exe
	-rm -f DocSimpleC.ps
	-rm interpreter	
distclean: clean
	-rm -f DocSimpleC.* LexSimpleC.* ParSimpleC.* LayoutSimpleC.* SkelSimpleC.* PrintSimpleC.* TestSimpleC.* AbsSimpleC.* TestSimpleC ErrM.* SharedString.* SimpleC.dtd XMLSimpleC.* Makefile*

