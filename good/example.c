//function uses global variable
void foo()
{	
	int a;
	a = 10;

	print(a * globalCounter);
	globalCounter -= 1;
	if (globalCounter > 0)
		foo();
}

int factorial(int n)
{   
   if (n == 1)
        return 1;
    return (factorial(n - 1) * n);
    print(666); // will be ignored because of return 
}

bool isGlobal;
int globalCounter;

void main() 
{	
	int a, i;
    print (factorial(6)); //720
	
	{
	    int a, b;
	    b = (145 % 100 - 12) / 2; //test for arithmetical operations
        b *= -3; //unary '-' and *=
	    print(b);//-18 

	    //isGlobal is initialized on default value "false"
	    print(!isGlobal); //check binary not
        print(a); //is initialized on default 0 value
	    a = 333;
	    //init depth of recursive call of foo by global variable
	    globalCounter = 5;	
	    foo();//prints 50 40 30 20 10

		print(a * 2);//666, that value of a wasn't chahged by foo() function
		if (b < a)//a = 666, b = 16
			if (a < 300)//nested if
                print(300)
            else
                print(700) //will print that value
		else
			print(false);
    }
    print(a);// on that scope a isn't assigned, so 0
    a = 4;
    while (a <= 300)
    {
    	print(a);//4 16 64 256
    	a *= 4;
    }
    
    for (i=0; i<6; i += 1)
    {
        print(i); //for i from 0 to 5
    }    
}